//
//  DetailViewModel.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 4/2/23.
//

import Foundation
import UIKit.UIColor

class DetailViewModel {
    var model: CellModel?
    
    func getType() -> InfoType {
        guard let type = model?.type else { return .empty }
        return type
    }
    
    func getTitle(_ hasColon: Bool = false) -> String? {
        switch getType() {
        case .character:
            return hasColon ? "\(LocalizedText.character) ID:" : LocalizedText.character
        case .episode:
            return hasColon ? "\(LocalizedText.episode) ID:" : LocalizedText.episode
        case .location:
            return hasColon ? "\(LocalizedText.location) ID:" : LocalizedText.location
        default:
            return nil
        }
    }
    
    func getStatusColor() -> UIColor? {
        guard let status = model?.status else { return nil }
        switch status {
        case .alive:
            return Color.alive
        case .dead:
            return Color.dead
        case .unknown:
            return Color.yellow
        }
    }
    
    func getID() -> String? {
        guard let id = self.model?.id else { return nil }
        return String(id)
    }
    
    func getExtraTitle() -> String? {
        switch getType() {
        case .character:
            return LocalizedText.genderTitle
        case .episode:
            return LocalizedText.episodeTitle
        case .location:
            return LocalizedText.dimensionTitle
        default:
            return nil
        }
    }
    
    func getExtra() -> String? {
        model?.getExtra()
    }
}
