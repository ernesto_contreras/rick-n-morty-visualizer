//
//  Locations.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 3/2/23.
//

import Foundation

struct LocationResponse: InfoProtocol, Codable {
    var info: Info?
    var results: [Location]?
}

struct Location: Codable {
    var id: Int?
    var name, type, dimension: String?
    var residents: [String]?
    var url: String?
    
    func toCellModel() -> CellModel {
        guard let name = self.name, let id = self.id else { return CellModel.defaultModel }
        let model = CellModel(
            name: name, type: .location, dimension: self.dimension, id: id
        )
        return model
    }
}
