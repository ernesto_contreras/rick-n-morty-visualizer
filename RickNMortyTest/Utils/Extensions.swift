//
//  Extensions.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 2/2/23.
//

import UIKit

extension Notification.Name {
    static let didChangeSegment = Notification.Name("didChangeSegment")
}

extension UIView {
    func roundView(_ radius: CGFloat = 12) {
        self.layer.cornerRadius = radius
    }
}

extension URLQueryItem {
    static func create(from values: [String: Any]) -> [URLQueryItem] {
        return values.flatMap { element -> [URLQueryItem] in
            let key = element.key
            let value = element.value
            if value is [String: Any] {
                return []
            }

            return [URLQueryItem(name: key, value: value as? String ?? "\(element.value)")]
        }
    }
}

extension UIViewController {
    enum ViewController: String {
        case detailVC = "DetailViewController"
    }
    
    func getVC(_ vc: ViewController) -> UIViewController {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: vc.rawValue)
        return viewController
    }
}

extension UICollectionView {
    func displayEmptyView(with message: String) {
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.frame.width - 48, height: self.frame.height))
        let lblMsg = UILabel(frame: rect)
        let messageLabel = lblMsg
        messageLabel.text = message
        messageLabel.textColor = Color.invertedAccent
        messageLabel.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        self.backgroundView = messageLabel
    }
}
