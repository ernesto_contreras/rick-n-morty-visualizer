//
//  CellModel.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 3/2/23.
//

import Foundation

enum InfoType: String {
    case character
    case episode
    case location
    case empty
}

struct CellModel {
    var name: String
    var type: InfoType
    var image: String?
    var gender: Gender?
    var status: Status?
    var location: String?
    var dimension: String?
    var episode: String?
    var species: String?
    var airDate: String?
    var firstEpisode: String?
    var id: Int
    
    func getEpisode() -> String? {
        guard let episode = self.firstEpisode?.last else { return nil }
        return "Episode \(episode)"
    }
    
    func getExtra() -> String? {
        switch type {
        case .character:
            return gender?.rawValue
        case .episode:
            return episode
        case .location:
            return dimension
        default:
            return nil
        }
    }
    
    static var defaultModel: CellModel {
        CellModel(name: "", type: .empty, id: 0)
    }
}
