//
//  HomeCollectionViewCell.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 4/2/23.
//

import UIKit
import Kingfisher

class HomeCollectionViewCell: UICollectionViewCell {

    // MARK: - Constant
    static let identifier = String(describing: HomeCollectionViewCell.self)

    @IBOutlet private(set) weak var containerView: UIView!
    @IBOutlet private(set) weak var gradientView: UIView!
    @IBOutlet private(set) weak var nameLabel: UILabel!
    @IBOutlet private(set) weak var extraLabel: UILabel!
    @IBOutlet private(set) weak var contentImageView: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        roundViews()
    }
    
    private func roundViews() {
        containerView.roundView()
        contentImageView.roundView()
        gradientView.roundView()
    }
    
    private func updateLabelsSize(_ shouldGrow: Bool) {
        nameLabel.font = UIFont.systemFont(ofSize: !shouldGrow ? 30 : 20, weight: .bold)
        extraLabel.isHidden = !shouldGrow
    }
    
    func setupCell(with model: CellModel) {
        
        contentImageView.kf.indicatorType = .activity
        self.nameLabel.text = model.name.capitalized
        
        switch model.type {
        case .character:
            let hasGender = model.gender != nil
            extraLabel.text = model.gender?.rawValue.capitalized
            updateLabelsSize(hasGender)
            if let image = model.image {
                contentImageView.kf.setImage(with: URL(string: image), placeholder: Image.loadingPortal)
            }
        case .location:
            let hasDimension = model.dimension != nil
            contentImageView.image = Image.location
            extraLabel.text = model.dimension?.capitalized
            updateLabelsSize(hasDimension)
        case .episode:
            let hasEpisode = model.episode != nil
            contentImageView.image = Image.episode
            extraLabel.text = model.episode?.capitalized
            updateLabelsSize(hasEpisode)
        case .empty:
            containerView.isHidden = true
        }
    }

}
