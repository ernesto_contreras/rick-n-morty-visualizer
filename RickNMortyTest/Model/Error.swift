//
//  Error.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 3/2/23.
//

import Foundation

struct ResponseErrorMessage: Codable {
    let error: String
}
