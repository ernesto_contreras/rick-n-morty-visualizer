//
//  HomeViewModel.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 2/2/23.
//

import Foundation

class HomeViewModel {
    // MARK: - Properties
    private var currentPage: Int = 1
    private var pagesLimit: Int?
    private var infoType: InfoType? {
        didSet {
            retrieveInfo()
        }
    }
    private var dataSource: [CellModel]? {
        didSet {
            updateDataSource()
        }
    }
    private var filteredDatasource: [CellModel]? {
        didSet {
            updateDataSource()
        }
    }
    private var shouldPaginate: Bool {
        guard let limit = self.pagesLimit else { return false }
        return limit > currentPage
    }
    private var isSearching: Bool = false
    
    // MARK: - Public
    var isFetching: Bool = false {
        didSet {
            guard !isSearching else { return }
            if isFetching, shouldPaginate {
                currentPage += 1
                retrieveInfo()
            }
        }
    }
    var resultsCompletion: (() -> Void)?
    var errorCompletion: ((String) -> Void)?
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateSegment(_:)), name: .didChangeSegment, object: nil)
    }
    
    func getResults() -> [CellModel] {
        if isSearching {
            guard let results = self.filteredDatasource else { return [] }
            return results
        } else {
            guard let results = self.dataSource else { return [] }
            return results
        }
    }
    
    private func getErrorMessage(from error: NetworkHandlerError) -> String {
        dataSource = nil
        filteredDatasource = nil
        resetResults()
        
        switch error {
        case .unknownError:
            return Errors.defaultError
        case .invalidURL:
            return Errors.urlError
        case .jsonDecodingError:
            return Errors.decodingError
        case .requestError(let message):
            return message
        }
    }
    
    func search(_ query: String) {
        isSearching = !query.isEmpty
        filteredDatasource = dataSource?.filter({
            $0.name.lowercased().contains(query.lowercased()) ||
            String($0.id).lowercased().contains(query.lowercased()) ||
            $0.gender?.rawValue.lowercased() == query.lowercased() ||
            $0.getExtra()?.lowercased().contains(query.lowercased()) ?? false
        })
    }
    
    func resetResults() {
        currentPage = 1
        pagesLimit = nil
        isSearching = false
    }
    
    func getType(from index: Int) -> InfoType {
        switch index {
        case 0:
            return .character
        case 1:
            return .episode
        case 2:
            return .location
        default:
            return .empty
        }
    }
    
    // MARK: - Helpers
    func updateDataSource() {
        isFetching = false
        resultsCompletion?()
    }
    
    private func retrieveInfo(_ params: Parameters? = nil) {
        switch infoType {
        case .character:
            fetchCharacters(params)
        case .episode:
            fetchEpisodes(params)
        case .location:
            fetchLocations(params)
        default:
            break
        }
    }
    
    private func createParameters(_ newParams: Parameters?) -> Parameters {
        if let newParams = newParams {
            return newParams
        } else {
            let params = Parameters(page: currentPage)
            return params
        }
    }
    
    // MARK: - API Calls
    private func fetchCharacters(_ params: Parameters? = nil) {
        NetworkManager.shared.request(type: CharacterResponse.self, url: API.charactersEndpoint(), params: createParameters(params)) { result in
            switch result {
            case .success(let response):
                guard let characters = response.results, let totalPages = response.info?.pages else {
                    self.errorCompletion?(self.getErrorMessage(from: .unknownError))
                    return
                }
                self.pagesLimit = totalPages
                if self.currentPage == 1 {
                    self.dataSource = characters.map({$0.toCellModel()})
                } else {
                    self.dataSource?.append(contentsOf: characters.map({$0.toCellModel()}))
                }
            case .failure(let error):
                self.errorCompletion?(self.getErrorMessage(from: error))
            }
        }
    }
    
    private func fetchEpisodes(_ params: Parameters? = nil) {
        NetworkManager.shared.request(type: EpisodeResponse.self, url: API.episodeEndpoint(), params: createParameters(params)) { result in
            switch result {
            case .success(let response):
                guard let episodes = response.results, let totalPages = response.info?.pages else {
                    self.errorCompletion?(self.getErrorMessage(from: .unknownError))
                    return
                }
                self.pagesLimit = totalPages
                
                if self.currentPage == 1 {
                    self.dataSource = episodes.map({$0.toCellModel()})
                } else {
                    self.dataSource?.append(contentsOf: episodes.map({$0.toCellModel()}))
                }
            case .failure(let error):
                self.errorCompletion?(self.getErrorMessage(from: error))
            }
        }
    }
    
    private func fetchLocations(_ params: Parameters? = nil) {
        NetworkManager.shared.request(type: LocationResponse.self, url: API.locationEndpoint(), params: createParameters(params)) { result in
            switch result {
            case .success(let response):
                guard let locations = response.results, let totalPages = response.info?.pages else {
                    self.errorCompletion?(self.getErrorMessage(from: .unknownError))
                    return
                }
                self.pagesLimit = totalPages
                
                if self.currentPage == 1 {
                    self.dataSource = locations.map({$0.toCellModel()})
                } else {
                    self.dataSource?.append(contentsOf: locations.map({$0.toCellModel()}))
                }
            case .failure(let error):
                self.errorCompletion?(self.getErrorMessage(from: error))
            }
        }
    }
    
    // MARK: - Notification action
    @objc private func updateSegment(_ notification: Notification) {
        guard let segment = notification.object as? Int else { return }
        resetResults()
        infoType = getType(from: segment)
    }
}
