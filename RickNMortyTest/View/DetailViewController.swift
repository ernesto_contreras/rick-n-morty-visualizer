//
//  DetailViewController.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 4/2/23.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {
    // MARK: - Constants
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var imageView: UIImageView!
    @IBOutlet private(set) weak var nameLabel: UILabel!
    @IBOutlet private(set) weak var statusStackView: UIStackView!
    @IBOutlet private(set) weak var statusTitleLabel: UILabel!
    @IBOutlet private(set) weak var statusLabel: UILabel!
    @IBOutlet private(set) weak var leftStackView: UIStackView!
    @IBOutlet private(set) weak var rightStackView: UIStackView!
    @IBOutlet private(set) weak var idStackView: UIStackView!
    @IBOutlet private(set) weak var idTitleLabel: UILabel!
    @IBOutlet private(set) weak var idLabel: UILabel!
    @IBOutlet private(set) weak var extraStackView: UIStackView!
    @IBOutlet private(set) weak var extraTitleLabel: UILabel!
    @IBOutlet private(set) weak var extraLabel: UILabel!
    @IBOutlet private(set) weak var speciesStackView: UIStackView!
    @IBOutlet private(set) weak var speciesTitleLabel: UILabel!
    @IBOutlet private(set) weak var speciesLabel: UILabel!
    @IBOutlet private(set) weak var locationStackView: UIStackView!
    @IBOutlet private(set) weak var locationTitleLabel: UILabel!
    @IBOutlet private(set) weak var locationLabel: UILabel!
    @IBOutlet private(set) weak var episodeStackView: UIStackView!
    @IBOutlet private(set) weak var episodeTitleLabel: UILabel!
    @IBOutlet private(set) weak var episodeLabel: UILabel!
    @IBOutlet private(set) weak var footerLabel: UILabel!

    // MARK: - Properties
    private var viewModel = DetailViewModel()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        setupUI()
    }
    
    private func setupUI() {
        nameLabel.text = viewModel.model?.name.uppercased()
        extraLabel.text = viewModel.getExtra()?.capitalized
        idLabel.text = viewModel.getID()
        if let image = viewModel.model?.image {
            imageView.kf.setImage(with: URL(string: image))
        }
        extraLabel.isHidden = viewModel.getExtra() == nil
        
        setupOptionalLabels()
        setupTitleLabels()
        
        leftStackView.alignment = viewModel.getType() == .character ? .leading : .center
        rightStackView.alignment = viewModel.getType() == .character ? .leading : .center
        nameLabel.textAlignment = viewModel.getType() == .character ? .left : .center
    }
    
    private func setupTitleLabels() {
        titleLabel.text = viewModel.getTitle()
        idTitleLabel.text = viewModel.getTitle(true)
        extraTitleLabel.text = viewModel.getExtraTitle()
        statusTitleLabel.text = LocalizedText.statusTitle
        speciesTitleLabel.text = LocalizedText.speciesTitle
        locationTitleLabel.text = LocalizedText.locationTitle
        episodeTitleLabel.text = LocalizedText.firstEpisodeTitle
        footerLabel.text = LocalizedText.wubba
    }
    
    private func setupOptionalLabels() {
        let status = viewModel.model?.status
        let species = viewModel.model?.species
        let location = viewModel.model?.location
        let episode = viewModel.model?.getEpisode()
        
        statusStackView.isHidden = status == nil
        statusLabel.text = status?.rawValue.capitalized
        statusLabel.textColor = viewModel.getStatusColor()
        
        speciesStackView.isHidden = species == nil
        speciesLabel.text = species?.capitalized
        
        locationStackView.isHidden = location == nil
        locationLabel.text = location?.capitalized
        
        episodeStackView.isHidden = episode == nil
        episodeLabel.text = episode?.capitalized
    }
    
    // MARK: - Helpers
    func setup(_ model: CellModel) {
        viewModel.model = model
    }
    
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
