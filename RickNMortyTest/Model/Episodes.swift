//
//  Episodes.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 3/2/23.
//

import Foundation

struct EpisodeResponse: InfoProtocol, Codable {
    var info: Info?
    var results: [Episode]?
}

struct Episode: Codable {
    var id: Int?
    var name, airDate, episode: String?
    var characters: [String]?
    var url: String?
    
    func toCellModel() -> CellModel {
        guard let name = self.name, let id = self.id else { return CellModel.defaultModel }
        let model = CellModel(
            name: name, type: .episode, episode: self.episode, airDate: self.airDate, id: id
        )
        return model
    }
}
