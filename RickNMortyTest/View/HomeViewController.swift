//
//  HomeViewController.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 2/2/23.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - Constants
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var segmentedControl: UISegmentedControl!
    @IBOutlet private(set) weak var collectionView: UICollectionView!
    @IBOutlet private(set) weak var searchView: SearchView!
    @IBOutlet private(set) weak var searchButtonContainerView: UIView!
    @IBOutlet private(set) weak var searchViewWidth: NSLayoutConstraint!
    
    // MARK: - Properties
    private var viewModel = HomeViewModel()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchView.roundView()
        collectionView.roundView()
        segmentedControl.roundView()
    }
    
    // MARK: - Setup
    private func setup() {
        setupUI()
        fetchInfo()
    }
    
    private func setupUI() {
        setupTexts()
        setupCollectionView()
        setupSearch()
    }
    
    private func fetchInfo() {
        NotificationCenter.default.post(name: .didChangeSegment, object: 0)
        manageResponse()
    }
    
    private func manageResponse() {
        viewModel.resultsCompletion = {
            DispatchQueue.main.async {
                self.collectionView.reloadSections(IndexSet(integer: 0))
            }
        }
        viewModel.errorCompletion = { error in
            DispatchQueue.main.async {
                SnackBarView().showSnackBar(message: error, style: .messageError, in: self)
            }
        }
    }
    
    private func setupTexts() {
        titleLabel.text = LocalizedText.title
        segmentedControl.setTitle(LocalizedText.characters, forSegmentAt: 0)
        segmentedControl.setTitle(LocalizedText.episodes, forSegmentAt: 1)
        segmentedControl.setTitle(LocalizedText.locations, forSegmentAt: 2)
        segmentedControl.setTitleTextAttributes([.foregroundColor: Color.invertedAccent, .font: UIFont.systemFont(ofSize: 16, weight: .semibold)], for: .normal)
        segmentedControl.setTitleTextAttributes([.foregroundColor: Color.accent, .font: UIFont.systemFont(ofSize: 20, weight: .semibold)], for: .selected)
    }
    
    private func setupCollectionView() {
        collectionView.register(UINib(nibName: HomeCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: HomeCollectionViewCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    private func setupSearch() {
        searchView.delegate = self
    }
    
    // MARK: - Helpers
    private func displaySearch(_ should: Bool) {
        performAnimation { [self] in
            searchViewWidth.constant = should ? UIScreen.main.bounds.width - 16 : 0
            titleLabel.alpha = should ? 0 : 1
            searchButtonContainerView.isHidden = should
            if should {
                searchView.textField.becomeFirstResponder()
            } else {
                searchView.reset()
            }
        }
    }
    
    private func performAnimation(_ completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0.5) { [unowned self] in
            completion()
            self.view.layoutIfNeeded()
          }
    }
    
    private func resetUI() {
        viewModel.resetResults()
        searchView.reset()
        displaySearch(false)
        
        if viewModel.getResults().count > 0 {
            collectionView.setContentOffset(.zero, animated: true)
        }
    }
    
    // MARK: - Actions
    @IBAction private func segmentControlChanged(_ sender: UISegmentedControl) {
        resetUI()
        NotificationCenter.default.post(name: .didChangeSegment, object: sender.selectedSegmentIndex)
        
    }

    @IBAction private func searchButtonTapped(_ sender: UIButton) {
        displaySearch(true)
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if !viewModel.getResults().isEmpty {
            collectionView.backgroundView = nil
            return viewModel.getResults().count
        } else {
            collectionView.displayEmptyView(
                with: "\(LocalizedText.emptyList) \(viewModel.getType(from: segmentedControl.selectedSegmentIndex).rawValue.capitalized)s found"
            )
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == viewModel.getResults().count - 1 {
            viewModel.isFetching = true
        }
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath)
            as? HomeCollectionViewCell {
            cell.setupCell(with: viewModel.getResults()[indexPath.row])
            return cell
        }
        
        return UICollectionViewCell()
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let detailVC = getVC(.detailVC) as? DetailViewController else { return }
        detailVC.setup(viewModel.getResults()[indexPath.row])
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (UIScreen.main.bounds.width / 2.15)
        return CGSize(width: size, height: size)
    }
}

extension HomeViewController: SearchViewDelegate {
    func didFinishSearching(_ text: String) {
        if text.isEmpty {
            displaySearch(false)
        }
        viewModel.search(text)
    }
}
