//
//  Info.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 3/2/23.
//

import Foundation

protocol InfoProtocol: Codable {
    var info: Info? { get set }
}

struct Info: Codable {
    let count, pages: Int?
    let next: String?
    let prev: String?
}

