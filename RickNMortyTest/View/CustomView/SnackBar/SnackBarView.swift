
import UIKit

class SnackBarView: UIView {

    // MARK: - Style
    enum Style {
        case messageSuccess
        case messageError
    }

    // MARK: - IBOutlets
    @IBOutlet private(set) weak var contentView: UIView!
    @IBOutlet private(set) weak var messageLabel: UILabel!
    @IBOutlet private(set) weak var messageView: UIView!
    @IBOutlet private(set) weak var iconImageView: UIImageView!
    
    // MARK: - Properties
    private var message: String = "" {
        didSet {
            self.messageLabel.text = message
        }
    }
    private var style: Style = .messageSuccess {
        didSet {
            setStyle()
        }
    }
    private var fadeInAnimator: UIViewPropertyAnimator?
    private var fadeOutAnimator: UIViewPropertyAnimator?
    private var snackBar: SnackBarView?

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    // MARK: - Helpers
    private func initialize() {
        Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
    }

    func setupUI() {
        self.contentView.layer.cornerRadius = 28
        self.iconImageView.layer.cornerRadius = 12
        self.contentView.clipsToBounds = true
        self.contentView.backgroundColor = Color.invertedAccent
    }

    // MARK: - Private
    private func setStyle() {
        switch style {
        case .messageSuccess:
            self.iconImageView.image = UIImage(systemName: "checkmark.shield.fill")
            self.iconImageView.tintColor = Color.alive
        case .messageError:
            self.iconImageView.image = UIImage(systemName: "xmark.shield.fill")
            self.iconImageView.tintColor = Color.dead
        }
        self.contentView.backgroundColor = UIColor(named: "blackTextLabel")
    }

    func configureSnackBar(in viewController: UIViewController, message: String, style: Style) {
        self.message = message
        self.style = style
        setupUI()
        guard let view = viewController.view else {
            return
        }

        contentView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(contentView)
        contentView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        contentView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        contentView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -32).isActive = true
        showView()
    }

    private func showView() {
        contentView.transform = CGAffineTransform(translationX: 0, y: 20)
        contentView.alpha = 0

        fadeInAnimator = UIViewPropertyAnimator(duration: 0.25, curve: .easeInOut) {
            self.contentView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.contentView.alpha = 1
        }

        fadeInAnimator?.addCompletion { _ in
            self.hiddenView(withDelay: 1.5)
            self.fadeInAnimator = nil
        }

        fadeInAnimator?.startAnimation()
    }

    private func hiddenView(withDelay delay: TimeInterval = 0.0) {
        var delay = delay

        if fadeOutAnimator != nil {
            delay = 0.0
            fadeOutAnimator?.stopAnimation(true)
        }

        fadeOutAnimator = UIViewPropertyAnimator(duration: 0.25, curve: .easeInOut) {
            if self.contentView != nil {
                self.contentView.transform = CGAffineTransform(translationX: 0, y: 20)
                self.contentView.alpha = 0
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.fadeOutAnimator?.startAnimation()
        }

        fadeOutAnimator?.addCompletion { _ in
            self.removeSnackBar()
        }
    }

    func forceCloseView() {
        self.removeSnackBar()
    }

    private func removeSnackBar() {
        if self.contentView != nil {
            self.contentView.removeFromSuperview()
            self.fadeOutAnimator = nil
        }
    }
}

extension SnackBarView {
    private func closeSnackView() {
        if let snackBar = self.snackBar {
            if snackBar.contentView != nil {
                snackBar.forceCloseView()
            }
        }
    }

    func showSnackBar(message: String, style: SnackBarView.Style, in vc: UIViewController) {
        closeSnackView()

        self.snackBar = SnackBarView(frame: .zero)
        self.snackBar?.configureSnackBar(in: vc, message: message, style: style)
    }
}
