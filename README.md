# Rick N Morty Visualizer



## What is it for

It's an app developed in Swift with UIKit. The architecture is MVVM, and it has a KingFisher integration for image caching. 

This app allows you to look through all Rick and Morty's available characters, episodes and locations. It displays detailed information about each type of information, and also has a search feature to filter based on the character, location or episode name. 

It also has infinite scroll and some cool (not really) animations. The dependency manager I used was Cocoapods.

The tools used on this project were: 
* [Rick and Morty's API](https://rickandmortyapi.com/documentation/)
* [Text generator with the series font](https://www.textstudio.com/logo/rick-and-morty-style-426)
* [Google for images searching](https://www.google.com/) (joke)

# What to do before compile
Make sure you run a `pod install` first. It has only one dependency, but still, make sure you don't forget this.

That's pretty much everything, so, enjoy.
