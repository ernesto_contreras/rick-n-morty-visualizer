//
//  Search.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 3/2/23.
//

import Foundation

struct Parameters {
    var page: Int?
    var id: Int?
    var ids: [Int]?
    var name, species, type: String?
    var status: Status?
    var gender: Gender?
    
    func toDictionary() -> [String: Any] {
        var dict: [String: Any] = [:]
        
        if let page = self.page {
            dict["page"] = page
        }
        
        if let id = self.id {
            dict["id"] = id
        }
        
        if let ids = self.ids {
            dict["ids"] = ids
        }
        
        if let name = self.name {
            dict["name"] = name
        }
        
        if let species = self.species {
            dict["species"] = species
        }
        
        if let type = self.type {
            dict["type"] = type
        }
        
        if let status = self.status {
            dict["status"] = status.rawValue
        }
        
        if let gender = self.gender {
            dict["gender"] = gender.rawValue
        }
        
        return dict
    }
}
