//
//  NetworkManager.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 3/2/23.
//

import Foundation

enum NetworkHandlerError: Error {
    case invalidURL
    case jsonDecodingError
    case requestError(String)
    case unknownError
    
    func getMessage() -> String {
        switch self {
        case .requestError(let error):
            return error
        case .jsonDecodingError:
            return Errors.decodingError
        default:
            return Errors.defaultError
        }
    }
}

class NetworkManager {
    static let shared = NetworkManager()
    private init() {}
    
    typealias ResponseCallback<T> = (Result<T, NetworkHandlerError>) -> Void
    
    func request<T: InfoProtocol>(type: T.Type, url: String, params: Parameters, completion: @escaping ResponseCallback<T>) {

        guard var components = URLComponents(string: url) else { return }
        components.queryItems = URLQueryItem.create(from: params.toDictionary())
        if let url = components.url {
            URLSession.shared.dataTask(with: URLRequest(url: url)) { data, _, error in
                if error == nil, let safeData = data {
                    do {
                        let decodedData = try JSONDecoder().decode(type, from: safeData)
                        completion(.success(decodedData))
                    } catch {
                        completion(.failure(.jsonDecodingError))
                    }
                } else {
                    if let safeData = data {
                        do {
                            let decodedError: ResponseErrorMessage = try JSONDecoder().decode(ResponseErrorMessage.self, from: safeData)
                            completion(.failure(.requestError(decodedError.error)))
                        } catch {
                            completion(.failure(.jsonDecodingError))
                        }
                    }
                }
            }.resume()
        }
    }
}
