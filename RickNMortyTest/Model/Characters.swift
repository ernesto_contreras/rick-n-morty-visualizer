//
//  Characters.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 3/2/23.
//

import Foundation

struct CharacterResponse: InfoProtocol, Codable {
    var info: Info?
    var results: [Characters]?
}

struct Characters: Codable {
    var id: Int?
    var name, species: String?
    var status: Status?
    var gender: Gender?
    var origin, location: Source?
    var image: String?
    var episode: [String]?
    var url: String?
    
    func toCellModel() -> CellModel {
        guard let name = self.name, let id = self.id else { return CellModel.defaultModel }
        let model = CellModel(
            name: name, type: .character, image: self.image, gender: self.gender,
            status: self.status, location: self.location?.name, species: self.species, firstEpisode: self.episode?.first, id: id
        )
        return model
    }
}

enum Status: String, Codable {
    case alive = "Alive"
    case dead = "Dead"
    case unknown
}

enum Gender: String, Codable {
    case male = "Male"
    case female = "Female"
    case genderless = "Genderless"
    case unknown = "unknown"
}

struct Source: Codable {
    var name: String?
    var url: String?
}
