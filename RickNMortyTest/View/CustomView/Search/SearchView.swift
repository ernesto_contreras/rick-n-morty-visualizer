//
//  SearchView.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 2/2/23.
//

import UIKit

protocol SearchViewDelegate: AnyObject {
    func didFinishSearching(_ text: String)
}

class SearchView: UIView {
    // MARK: - Outlet
    @IBOutlet private(set) weak var contentView: UIView!
    @IBOutlet private(set) weak var textField: UITextField!
    @IBOutlet private(set) weak var iconImageView: UIImageView!
    private var isTyping: Bool = false {
        didSet {
            updateIcon()
        }
    }
    
    weak var delegate: SearchViewDelegate?
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // MARK: - Helpers
    private func initialize() {
        Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
        addSubview(self.contentView)
        self.backgroundColor = .clear
        self.contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setupUI()
    }
    
    private func setupUI() {
        setupTextField()
    }
    
    private func setupTextField() {
        textField.delegate = self
        textField.attributedPlaceholder = NSAttributedString(string: LocalizedText.searchPlaceholder, attributes: [.foregroundColor: Color.accent.withAlphaComponent(0.5)])
        textField.textColor = Color.accent
    }
    
    func reset() {
        textField.text = nil
        isTyping = false
        textField.resignFirstResponder()
    }
    
    private func updateIcon() {
        iconImageView.image = isTyping ? Image.close : Image.search
    }

    @IBAction private func searchButtonTapped(_ sender: UIButton) {
        if isTyping {
            isTyping = false
            textField.text?.removeAll()
            textField.resignFirstResponder()
        }
        if let text = textField.text {
            delegate?.didFinishSearching(text)
        }
    }
}


extension SearchView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let text = textField.text, !text.isEmpty {
            isTyping = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text {        
            delegate?.didFinishSearching(text)
            isTyping = false
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let safeText = textField.text, let textRange = Range(range, in: safeText),
                   let updatedText = safeText.replacingCharacters(in: textRange,
                                                              with: string) as String? {
            isTyping = !updatedText.isEmpty
        }
        return true
    }
}
