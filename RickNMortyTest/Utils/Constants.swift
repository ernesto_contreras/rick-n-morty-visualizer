//
//  Constants.swift
//  RickNMortyTest
//
//  Created by Ernesto Jose Contreras Lopez on 2/2/23.
//

import UIKit

struct API {
    static let baseUrl = "https://rickandmortyapi.com/api"

    struct Endpoint {
        static let location = "/location"
        static let character = "/character"
        static let episode = "/episode"
    }
    
    static func charactersEndpoint() -> String {
        baseUrl + Endpoint.character
    }
    
    static func locationEndpoint() -> String {
        baseUrl + Endpoint.location
    }
    
    static func episodeEndpoint() -> String {
        baseUrl + Endpoint.episode
    }
}

// MARK: - Text
struct LocalizedText {
    static let title = "Rick n Morty Visualizer"
    static let searchPlaceholder = "Look for any character, episode or location here."
    static let characters = "Characters"
    static let locations = "Locations"
    static let episodes = "Episodes"
    static let wubba = "© Wubba Lubba Dub Dub! ®"
    
    static let character = "Character"
    static let location = "Location"
    static let episode = "Episode"
    
    static let genderTitle = "Gender"
    static let episodeTitle = "Season"
    static let dimensionTitle = "Dimension"
    static let speciesTitle = "Species"
    static let statusTitle = "Status"
    static let firstEpisodeTitle = "First seen in"
    static let locationTitle = "Last known location"
    
    static let emptyList = "There were zero"
}

// MARK: - Color
struct Color {
    static let brown = UIColor(named:"brownColor") ?? UIColor()
    static let yellow = UIColor(named:"yellowColor") ?? UIColor()
    static let green = UIColor(named:"greenColor") ?? UIColor()
    static let pink = UIColor(named:"pinkColor") ?? UIColor()
    static let skin = UIColor(named:"skinColor") ?? UIColor()
    static let blue = UIColor(named:"blueColor") ?? UIColor()
    static let accent = UIColor(named:"AccentColor") ?? UIColor()
    static let invertedAccent = UIColor(named:"InvertedAccentColor") ?? UIColor()
    static let darkBlue = UIColor(named:"darkBlueColor") ?? UIColor()
    static let alive = UIColor(named:"aliveColor") ?? UIColor()
    static let dead = UIColor(named:"deadColor") ?? UIColor()
}

struct Image {
    static let search = UIImage(named: "search")
    static let close = UIImage(named: "close")
    static let location = UIImage(named: "location")
    static let episode = UIImage(named: "episode")
    static let loadingPortal = UIImage(named: "portal")
}

struct Errors {
    static let defaultError = "An error has occured. Please try again."
    static let decodingError = "There was an error trying to decode your data."
    static let urlError = "There was an error trying to connect to your URL."
}
